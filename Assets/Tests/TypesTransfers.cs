using Jineni.Personality.types;
using NUnit.Framework;
using UnityEngine;
using static Jineni.Personality.PersonalityType;

public class TypesTransfers
{
    // A Test behaves as an ordinary method
    [Test]
    public void TypesTransfersFromMBT()
    {
        MBTFunctionsName[] functions;
        MBTPersonalityType personalityType;

        functions = new MBTFunctionsName[4] {
                MBTFunctionsName.E,
                MBTFunctionsName.N,
                MBTFunctionsName.T,
                MBTFunctionsName.J
            };
        //string typeName = string.Concat(functions);
        personalityType = new MBTPersonalityType(functions);
        Debug.Log(personalityType.name + ": " + GetCogFromMBT(personalityType));
        Assert.AreEqual("TeNiSeFi", GetCogFromMBT(personalityType));

        functions = new MBTFunctionsName[4] {
                MBTFunctionsName.I,
                MBTFunctionsName.N,
                MBTFunctionsName.T,
                MBTFunctionsName.J
            };
        //string typeName = string.Concat(functions);
        personalityType = new MBTPersonalityType(functions);
        Debug.Log(personalityType.name + ": " + GetCogFromMBT(personalityType));
        Assert.AreEqual("NiTeFiSe", GetCogFromMBT(personalityType));

        functions = new MBTFunctionsName[4] {
                MBTFunctionsName.I,
                MBTFunctionsName.N,
                MBTFunctionsName.T,
                MBTFunctionsName.P
            };
        //string typeName = string.Concat(functions);
        personalityType = new MBTPersonalityType(functions);
        Debug.Log(personalityType.name + ": " + GetCogFromMBT(personalityType));
        Assert.AreEqual("TiNeSiFe", GetCogFromMBT(personalityType));

        functions = new MBTFunctionsName[4] {
                MBTFunctionsName.I,
                MBTFunctionsName.N,
                MBTFunctionsName.F,
                MBTFunctionsName.J
            };
        //string typeName = string.Concat(functions);
        personalityType = new MBTPersonalityType(functions);
        Debug.Log(personalityType.name + ": " + GetCogFromMBT(personalityType));
        Assert.AreEqual("NiFeTiSe", GetCogFromMBT(personalityType));
    }

    [Test]
    public void TypesTransfersFromCog()
    {
        CogFunctionName[] functions;
        CogPersonalityType personalityType;

        functions = new CogFunctionName[4] {
                CogFunctionName.Ni,
                CogFunctionName.Fe,
                CogFunctionName.Ti,
                CogFunctionName.Se
            };
        //string typeName = string.Concat(functions);
        personalityType = new CogPersonalityType(functions);
        Debug.Log(personalityType.name + ": " + GetMBTFromCog(personalityType));
        Assert.AreEqual("INFJ", GetMBTFromCog(personalityType));
        
    }

}
