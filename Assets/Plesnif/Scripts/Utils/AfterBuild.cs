using System.Diagnostics;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;


#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.Callbacks;
#endif
namespace plesnif.utils
{
    public class AfterBuild
    {
        /*
        [MenuItem("AfterBuild/Copy App Configs")]
        public static void GetFilesToCopy()
        {
            string path = EditorUtility.SaveFolderPanel("Choose Location of Built Game", "", "");

            //UnityEngine.Debug.Log(path);

            FileUtil.CopyFileOrDirectory("Assets/Configs", path + "/Configs");

            string[] filePaths = Directory.GetFiles(path + "/Configs", "*.meta", SearchOption.AllDirectories);

            int index = Application.dataPath.Length;
            for (int i = 0; i < filePaths.Length; i++)
            {
                FileUtil.DeleteFileOrDirectory(filePaths[i]);
            }

            UnityEngine.Debug.Log("app configs copied");
        }*/
    }
    /*
    class MyCustomBuildProcessor : IPostprocessBuild
    {
        public int callbackOrder { get { return 0; } }
        public void OnPostprocessBuild(BuildTarget target, string path)
        {
            File.Copy("sourceFilePath", "destinationFilePath");
        }
    }*/

#if UNITY_EDITOR
    [PostProcessBuild]
    class PreBuild: IPostprocessBuildWithReport
    {
        public int callbackOrder { get { return 0; } }
        public void OnPostprocessBuild(BuildReport report)
        {
            string rootProjectFolder = Path.Combine(Application.dataPath, "../");
            string sourceFolder = Path.Combine(rootProjectFolder, "Configs/");
            string rootBuildFolder = Path.Combine(report.summary.outputPath, "../");
            string targetFolder = Path.Combine(rootBuildFolder, "Configs/");

            FileUtil.CopyFileOrDirectory(sourceFolder, targetFolder);

            string[] filePaths = Directory.GetFiles(targetFolder, "*.meta", SearchOption.AllDirectories);

            int index = Application.dataPath.Length;
            for (int i = 0; i < filePaths.Length; i++)
            {
                FileUtil.DeleteFileOrDirectory(filePaths[i]);
            }

            UnityEngine.Debug.Log("app configs copied");
        }

       
        /*
        public void OnPreprocessBuild(BuildReport report)
        {
            if (PlayerSettings.bundleVersion != String.Format("{0:yyMMdd}", System.DateTime.Now))
            {
                throw new UnityEditor.Build.BuildFailedException("build verze nesedi s dneskem");
                //UnityEngine.Debug.LogError();
            }
        }*/
    }

    /*
    public static class PostBuild
    {

        // Automatically increment the version number for each unity build. Particularly useful so you can push Unity Cloud Build projects to prod with a peace of mind.
        [PostProcessBuild]
        public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
        {
            if (PlayerSettings.bundleVersion != String.Format("{0:yyMMdd}", System.DateTime.Now))
            {
                throw new Exception("build verze nesedi s dneskem");
            }


        }

    }*/

#endif
}
