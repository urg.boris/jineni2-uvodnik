using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

namespace plesnif.utils
{
    public class ConfigLoader
    {
        public string configsDirPath = "";
        public ConfigLoader(string loaderDirPath)
        {
            configsDirPath = loaderDirPath;
        }

        /*  public static string configsPathEditor = "Assets/Configs/";
          public static string configsPathBuild = "../Configs/";
          public static string defaultAppConfigFile = "AppConfig.json";*/
        //public static string tutorialConfigFile = "tutorial.json";

        public string GetPath()
        {
            if (configsDirPath == "")
            {
                Debug.LogError("dir for config loader not set");
            }
            return configsDirPath;
        }

        public string GetFileInConfigDirPath(string fileName)
        {
            return Path.Combine(GetPath(), fileName);
        }
/*
        T LoadJson<T>(string fileName)
        {
            string path = GetFileInConfigDirPath(fileName);
            if (!File.Exists(path))
            {
                Debug.LogError("cant load file: " + path);

            }
            StreamReader reader = new StreamReader(path);
            string json = reader.ReadToEnd();
            T loadedObject = JsonUtility.FromJson<T>(json);

            //Debug.Log(LevelConfig.allowedHeliportIndexes[0]);
            reader.Close();
            return loadedObject;
        }*/

        string LoadJson(string path)
        {
          //  Debug.Log("LoadJson"+path);
            if (!File.Exists(path))
            {
                Debug.LogError("cant load file: " + path);

            }
            StreamReader reader = new StreamReader(path);
            string json = reader.ReadToEnd();

            reader.Close();
            return json;
        }

        public void LoadConfig<T>(T appConfig, string fileName)
        {
         //   Debug.Log(GetPath());
        //    Debug.Log(fileName);
            string path = GetPath() + fileName;
         //   Debug.Log(path);
            if (appConfig == null)
            {
                Debug.LogError("config cant be loaded: config object not set");
            }
            try
            {
                Debug.Log("Config loaded: " + path);
                JsonUtility.FromJsonOverwrite(LoadJson(path), appConfig);
            }
            catch (Exception e)
            {
                Debug.Log("Creating empty " + path);
                SaveConfig(appConfig, fileName);
            }
            //Debug.Log(JsonUtility.ToJson(ScriptableObject.CreateInstance<LevelConfig>()));
        }


        public void SaveConfig<T>(T appConfig, string fileName)
        {
            //Debug.Log("file " + fileName + " saved");
            string path = GetPath() + fileName;
            SaveJson(path, appConfig);
        }

        public void SaveJson<T>(string path, T objectToSave)
        {
            string stringObject = JsonUtility.ToJson(objectToSave, true);
            if (!File.Exists(path))
            {
                FileStream newFile = new System.IO.FileStream(path, System.IO.FileMode.Create);
                newFile.Close();
            }
            File.WriteAllText(path, stringObject);
            Debug.Log("saved " + path);
        }
    }
}
