using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace plesnif.utils
{

    public static class Enviroment
    {
        public const string APP_CONFIG_FILENAME = "appConfig.json";
        /*
        private static ScriptableObject appConfig;
        public static ScriptableObject AppConfig
        {
            get
            {
                if (appConfig == null)
                {
#if UNITY_EDITOR
                    EditorHelpers.LoadAppConfig<AppConfig>();
#else
                    //TODO??probably loaded by runtime
#endif
                }
                return appConfig;
            }
            set => appConfig = value;
        }*/
        /*
        public static string GetUrlPath(string relativeUrl)
        {
            return Path.Combine(AppConfig.urlPath, relativeUrl);
        }

        public static string GetSavePath(string relativePath)
        {
            return Path.Combine(AppConfig.savePath, relativePath);
        }*/

        public static string GetAppConfigPath()
        {
            return GetConfigsPath() + APP_CONFIG_FILENAME;
        }


        public static string GetConfigsPath()
        {
#if !UNITY_EDITOR
        return  Application.dataPath + "/../Configs/" ;
#endif
#if UNITY_EDITOR
            return Application.dataPath + "/../Configs/";
#endif
#if UNITY_IOS
        return "";
#endif
        }
    }
}
