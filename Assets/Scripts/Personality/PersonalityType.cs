using Jineni.Personality.types;
using Jineni.Preface.Structure;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Jineni.Personality
{
    public class PersonalityType
    {
        public const int PERSONALITY_AREAS_COUNT = 8;
        /* public static string[] COG_FUNCTION_NAMES = new string[PERSONALITY_AREAS_COUNT] { "Ne", "Fe", "Te", "Se", "Ni", "Fi", "Ti", "Si" };
         public static string[] MBT_LETTERS_NAMES = new string[PERSONALITY_AREAS_COUNT] { "I", "E", "N", "S", "T", "F", "J", "P" };
         */
        public enum CogFunctionName
        {
            Ne, Fe, Te, Se, Ni, Fi, Ti, Si,
        }

        public enum MBTFunctionsName
        {
            I, E, N, S, T, F, J, P,
        }

        public MBTFunctionValues mbtFunctionsValues = new MBTFunctionValues();
        public CogFunctionValues cogFunctionsValues = new CogFunctionValues();
        /*
                const string typeToInstantiate_prefix = "Jineni.Personality.types.";
                static string typeToInstantiate_sufix = ", Assembly - CSharp";

                public PersonalityType()
                {
                   // typeToInstantiate_sufix = typeof(PersonalityType).AssemblyQualifiedName;
                }



                public static string GetTypeToInstantiateName(string className)
                {
                    return typeToInstantiate_prefix + className + typeToInstantiate_sufix;
                }*/

        public static CogFunctionName StringToCogName(string name)
        {
            return (CogFunctionName)System.Enum.Parse(typeof(CogFunctionName), name);
        }

        public static MBTFunctionsName StringToMBTName(string name)
        {
            return (MBTFunctionsName)System.Enum.Parse(typeof(MBTFunctionsName), name);
        }

        //from https://www.reddit.com/r/mbti/comments/102fb6/howto_convert_between_fourletter_types_and/
        public static string GetCogFromMBT(MBTPersonalityType personalityType)
        {


            CogFunctionName[] functionNames = new CogFunctionName[4];

            bool isThirdLLetterExtraverted = personalityType.position4 == MBTFunctionsName.J;
            bool isFirstFunctionExtraverted = personalityType.position1 == MBTFunctionsName.E;

            if (isFirstFunctionExtraverted)
            {
                if (isThirdLLetterExtraverted)
                {
                    functionNames[0] = StringToCogName(personalityType.position3 + "e");
                    functionNames[1] = StringToCogName(personalityType.position2 + "i");
                }
                else
                {
                    functionNames[0] = StringToCogName(personalityType.position2 + "e");
                    functionNames[1] = StringToCogName(personalityType.position3 + "i");
                }
            }
            else
            {
                if (isThirdLLetterExtraverted)
                {
                    functionNames[0] = StringToCogName(personalityType.position2 + "i");
                    functionNames[1] = StringToCogName(personalityType.position3 + "e");
                }
                else
                {
                    functionNames[0] = StringToCogName(personalityType.position3 + "i");
                    functionNames[1] = StringToCogName(personalityType.position2 + "e");
                }
            }
            functionNames[2] = CogFunctionType.GetOpposing(functionNames[1]);
            functionNames[3] = CogFunctionType.GetOpposing(functionNames[0]);

            return string.Concat(functionNames);
        }

        public static bool IsCogFunctionExtraverted(CogFunctionName function)
        {
            return function.ToString().EndsWith("e");
        }

        public static MBTFunctionsName CogFunctionToMBT(CogFunctionName function)
        {
            return StringToMBTName(function.ToString()[0].ToString());
        }

        //from https://www.reddit.com/r/mbti/comments/102fb6/howto_convert_between_fourletter_types_and/
        public static string GetMBTFromCog(CogPersonalityType personalityType)
        {
            //CogPersonalityType personalityType = functionsValues.GetMostProminentPersonType();
            MBTFunctionsName[] functionNames = new MBTFunctionsName[4];

            bool isFirstFunctionExtraverted = IsCogFunctionExtraverted(personalityType.position1);
            bool shouldMiddleLettersSwitch = isFirstFunctionExtraverted; // !IsCogFunctionExtraverted(personalityType.position3);

            functionNames[0] = isFirstFunctionExtraverted ? MBTFunctionsName.E : MBTFunctionsName.I;
            functionNames[3] = shouldMiddleLettersSwitch ? MBTFunctionsName.P : MBTFunctionsName.J;

            if (shouldMiddleLettersSwitch)
            {
                functionNames[1] = CogFunctionToMBT(personalityType.position2);
                functionNames[2] = CogFunctionToMBT(personalityType.position1);
            }
            else
            {
                functionNames[1] = CogFunctionToMBT(personalityType.position1);
                functionNames[2] = CogFunctionToMBT(personalityType.position2);
            }

            return string.Concat(functionNames);
        }



        public void BoostMBTFunction(MBTFunctionsName name, float value)
        {
            mbtFunctionsValues.BoostFunction(name, value);
        }

        public void BoostCogFunction(CogFunctionName name, float value)
        {
            cogFunctionsValues.BoostFunction(name, value);
        }

        public void BoostFunctions(PrefaceFunctionBoost[] functionBoost)
        {
            for (int i = 0; i < functionBoost.Length; i++)
            {
                string functionNameString = functionBoost[i].name;
                bool isCogFunction = functionNameString.Length > 1;
                if (isCogFunction)
                {

                    var functionName = StringToCogName(functionNameString);
                    var function = cogFunctionsValues.GetFunctionByName(functionName);
                    function.BoostIntensity(functionBoost[i].value);
                }
                else
                {
                    var functionName = StringToMBTName(functionNameString);
                    var function = mbtFunctionsValues.GetFunctionByName(functionName);
                    function.BoostIntensity(functionBoost[i].value);
                }
            }
        }

        public

        void DebugPersonality(MBTPersonalityType personality)
        {
            Debug.Log("MBT personality: " + personality.name + " = " + GetCogFromMBT(personality));
        }

        void DebugPersonality(CogPersonalityType personality)
        {
            Debug.Log("Cog personality: " + personality.name + " = " + GetMBTFromCog(personality));
        }

        public void Do()
        {

            mbtFunctionsValues.BoostFunction(MBTFunctionsName.E);
            mbtFunctionsValues.BoostFunction(MBTFunctionsName.J);
            MBTPersonalityType mBTPersonalityType = mbtFunctionsValues.GetMostProminentPersonType();
            DebugPersonality(mBTPersonalityType);

            cogFunctionsValues.BoostFunction(CogFunctionName.Fe);
            cogFunctionsValues.BoostFunction(CogFunctionName.Fe);
            cogFunctionsValues.BoostFunction(CogFunctionName.Te);
            cogFunctionsValues.BoostFunction(CogFunctionName.Ne);
            cogFunctionsValues.BoostFunction(CogFunctionName.Ni);
            CogPersonalityType cogPersonalityType = cogFunctionsValues.GetMostProminentPersonType();
            DebugPersonality(cogPersonalityType);



        }
    }
}
