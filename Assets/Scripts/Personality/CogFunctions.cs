using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using static Jineni.Personality.PersonalityType;
using System.Linq;
using Jineni.Personality.types;

namespace Jineni.Personality
{

    public class CogFunctionValues
    {
        //public CogFunction[] list = new CogFunction[FUNCTIONS_COUNT];
        public Dictionary<CogFunctionName, CogFunction> list;
        CogFunctionType currentlyDominantFunction;
        CogFunctionType currentlySecondFunction;

        public CogFunctionValues()
        {
            this.list = CogFunctionFactory.CreateList();
        }

        public CogFunction GetFunctionByName(CogFunctionName name)
        {
            if (!list.ContainsKey(name))
            {
                string errMsg = "no " + name + " function in list";
                Debug.LogError(errMsg);
                throw new Exception(errMsg);
            }
            return list[name];
        }

        public void BoostFunctionRandomly(CogFunctionName name)
        {
            GetFunctionByName(name).BoostRamdomly();
        }

        public void BoostFunction(CogFunctionName name, float value=-1)
        {
            if (value == -1)
            {
                value = UnityEngine.Random.value;
            }
            GetFunctionByName(name).BoostIntensity(value);
        }

        
        public CogFunctionName GetPosition1()
        {
            CogFunctionName name = list.
                OrderByDescending(x => x.Value.intensity).
                FirstOrDefault().Key;

            currentlyDominantFunction = new CogFunctionType(name);
            return name;
        }

        public CogFunctionName GetPosition2()
        {
            CogFunctionName[] possibleNames = currentlyDominantFunction.GetPossibleSecondFunctions();
            CogFunctionName name = list.
                OrderByDescending(x => x.Value.intensity).
                Where(x => x.Key == possibleNames[0] || x.Key == possibleNames[1])
                .FirstOrDefault().Key;
            currentlySecondFunction = new CogFunctionType(name);
            return name;
        }
        public CogFunctionName GetPosition3()
        {
            return CogFunctionType.GetOpposing(currentlySecondFunction.name);
        }
        public CogFunctionName GetPosition4()
        {
            return CogFunctionType.GetOpposing(currentlyDominantFunction.name);
        }

        public CogPersonalityType GetMostProminentPersonType()
        {
            CogFunctionName[] functions = new CogFunctionName[4] {
                GetPosition1(),
                GetPosition2(),
                GetPosition3(),
                GetPosition4()
            };

            return new CogPersonalityType(functions);
        }
    }

    public abstract class CogFunction
    {
        public int index;
        public float intensity;
        public CogFunction(int index, int value)
        {
            this.index = index;
            this.intensity = value;
        }

        public void BoostIntensity(float value)
        {
            intensity += value;
        }

        public void BoostRamdomly()
        {
            intensity += UnityEngine.Random.value;
        }


    }

    public static class CogFunctionFactory
    {
        public static Dictionary<CogFunctionName, CogFunction> CreateList()
        {
            var possibleNames = System.Enum.GetNames(typeof(CogFunctionName));

            return new Dictionary<CogFunctionName, CogFunction>
            {                
                { CogFunctionName.Ne, new Ne() },
                { CogFunctionName.Fe, new Fe() },
                { CogFunctionName.Te, new Te() },
                { CogFunctionName.Se, new Se() },
                { CogFunctionName.Ni, new Ni() },
                { CogFunctionName.Fi, new Fi() },
                { CogFunctionName.Ti, new Ti() },
                { CogFunctionName.Si, new Si() },
            };
        }
    }


    public class Ne : CogFunction
    {
        public const int INDEX = 0;
        public Ne(int value = 0) : base(INDEX, value)
        {
        }
    }
    public class Fe : CogFunction
    {
        public const int INDEX = 1;
        public Fe(int value = 0) : base(INDEX, value)
        {
        }
    }
    public class Te : CogFunction
    {
        public const int INDEX = 2;

        public Te(int value = 0) : base(INDEX, value)
        {
        }
    }
    public class Se : CogFunction
    {
        public const int INDEX = 3;

        public Se(int value = 0) : base(INDEX, value)
        {
        }
    }
    public class Ni : CogFunction
    {
        public const int INDEX = 4;
        public Ni(int value = 0) : base(INDEX, value)
        {
        }
    }
    public class Fi : CogFunction
    {
        public const int INDEX = 5;
        public Fi(int value = 0) : base(INDEX, value)
        {
        }
    }
    public class Ti : CogFunction
    {
        public const int INDEX = 6;
        public Ti(int value = 0) : base(INDEX, value)
        {
        }
    }
    public class Si : CogFunction
    {
        public const int INDEX = 7;
        public Si(int value = 0) : base(INDEX, value)
        {

        }
    }
}