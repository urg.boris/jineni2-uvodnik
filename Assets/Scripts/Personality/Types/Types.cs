﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Jineni.Personality.PersonalityType;

namespace Jineni.Personality.types
{
    public class CogFunctionType
    {
        public const int POSSIBILITIES_FOR_SECOND_COUNT = 2;

        public CogFunctionName name;
        public CogFunctionName inferiorName;
        public CogFunctionType(CogFunctionName name)
        {
            this.name = name;
        }

        public CogFunctionName GetOpposing()
        {
            return GetOpposing(name);
        }

        public CogFunctionName[] GetPossibleSecondFunctions()
        {
            return GetPossibleSecondFunctions(name);
        }

        public static CogFunctionName[] GetPossibleSecondFunctions(CogFunctionName name)
        {
            CogFunctionName[] possibleSecondFunction = new CogFunctionName[POSSIBILITIES_FOR_SECOND_COUNT];
            switch (name)
            {
                case CogFunctionName.Ne:
                case CogFunctionName.Se:
                    possibleSecondFunction[0] = CogFunctionName.Ti;
                    possibleSecondFunction[1] = CogFunctionName.Fi;
                    break;
                case CogFunctionName.Fe:
                case CogFunctionName.Te:
                    possibleSecondFunction[0] = CogFunctionName.Si;
                    possibleSecondFunction[1] = CogFunctionName.Ni;
                    break;


                case CogFunctionName.Ni:
                case CogFunctionName.Si:
                    possibleSecondFunction[0] = CogFunctionName.Te;
                    possibleSecondFunction[1] = CogFunctionName.Fe;
                    break;
                case CogFunctionName.Fi:
                case CogFunctionName.Ti:
                    possibleSecondFunction[0] = CogFunctionName.Se;
                    possibleSecondFunction[1] = CogFunctionName.Ne;
                    break;
                default:
                    throw new Exception("no type");
            }
            return possibleSecondFunction;
        }

        public static CogFunctionName GetOpposing(CogFunctionName name)
        {
            switch(name)
            {
                case CogFunctionName.Ne:
                    return CogFunctionName.Si;
                case CogFunctionName.Se:
                    return CogFunctionName.Ni;
                case CogFunctionName.Fe:
                    return CogFunctionName.Ti;
                case CogFunctionName.Te:
                    return CogFunctionName.Fi;

                case CogFunctionName.Ni:
                    return CogFunctionName.Se;
                case CogFunctionName.Si:
                    return CogFunctionName.Ne;
                case CogFunctionName.Fi:
                    return CogFunctionName.Te;
                case CogFunctionName.Ti:
                    return CogFunctionName.Fe;
                default:
                    throw new Exception("no type");

            }
        }
    }


    public class Ne : CogFunctionType
    {
        public Ne(CogFunctionName name) : base(name)
        {
        }
    }
    public class Fe : CogFunctionType
    {
        public Fe(CogFunctionName name) : base(name)
        {
        }
    }
    public class Te : CogFunctionType
    {
        public Te(CogFunctionName name) : base(name)
        {
        }
    }
    public class Se : CogFunctionType
    {
        public Se(CogFunctionName name) : base(name)
        {
        }
    }
    public class Ni : CogFunctionType
    {
        public Ni(CogFunctionName name) : base(name)
        {
        }
    }
    public class Fi : CogFunctionType
    {
        public Fi(CogFunctionName name) : base(name)
        {
        }
    }
    public class Ti : CogFunctionType
    {
        public Ti(CogFunctionName name) : base(name)
        {
        }
    }
    public class Si : CogFunctionType
    {
        public Si(CogFunctionName name) : base(name)
        {
        }
    }


    public abstract class APersonalitySystemType
    {
        public string name;

        public APersonalitySystemType()
        {
            //this.name = string.Concat(name);
        }
    }
    public class MBTPersonalityType : APersonalitySystemType
    {
        public MBTFunctionsName position1;
        public MBTFunctionsName position2;
        public MBTFunctionsName position3;
        public MBTFunctionsName position4;
        public MBTPersonalityType(MBTFunctionsName[] functions) 
        {
            position1 = functions[0];
            position2 = functions[1];
            position3 = functions[2];
            position4 = functions[3];
            name = string.Concat(functions);
        }
    }

    public class CogPersonalityType : APersonalitySystemType
    {
        public CogFunctionName position1;
        public CogFunctionName position2;
        public CogFunctionName position3;
        public CogFunctionName position4;
        public CogPersonalityType(CogFunctionName[] functions)
        {
            position1 = functions[0];
            position2 = functions[1];
            position3 = functions[2];
            position4 = functions[3];
            name = string.Concat(functions);
        }
    }
    /*
    public class INTP : MBTPersonalityType {

    }
    public class ISTP : MBTPersonalityType
    {

    }
    public class ENTP : MBTPersonalityType
    {

    }
    public class ENFP : MBTPersonalityType
    {

    }
    public class INFP : MBTPersonalityType
    {

    }
    public class INTJ : MBTPersonalityType
    {

    }
    public class INFJ : MBTPersonalityType
    {

    }
    public class ESTJ : MBTPersonalityType
    {

    }

    public class ENTJ : MBTPersonalityType
    {

    }
    public class ESFJ : MBTPersonalityType
    {

    }
    public class ENFJ : MBTPersonalityType
    {

    }
    public class ISTJ : MBTPersonalityType
    {

    }
    public class ISFJ : MBTPersonalityType
    {

    }
    public class ESTP : MBTPersonalityType
    {

    }
    public class ESFP : MBTPersonalityType
    {

    }

    */



    /*
    public class TiNeSiFe : CogPersonalityType
    {

    }
    public class TiSeNiFe : CogPersonalityType
    {

    }
    public class NeTiFeSi : CogPersonalityType
    {

    }
    public class NeFiTeSi : CogPersonalityType
    {

    }
    public class FiSeNiTe : CogPersonalityType
    {

    }
    public class FiNeSiTe : CogPersonalityType
    {

    }
    public class NiTeFiSe : CogPersonalityType
    {

    }
    public class NiFeTiSe : CogPersonalityType
    {

    }

    public class TeSiNeFi : CogPersonalityType
    {

    }
    public class TeNiSeFi : CogPersonalityType
    {

    }
    public class FeSiNeTi : CogPersonalityType
    {

    }
    public class FeNiSeTi : CogPersonalityType
    {

    }
    public class SiTeFiNe : CogPersonalityType
    {

    }
    public class SiFeTiNe : CogPersonalityType
    {

    }
    public class SeTiFeNi : CogPersonalityType
    {

    }
    public class SeFiTeNi : CogPersonalityType
    {

    }*/
}
