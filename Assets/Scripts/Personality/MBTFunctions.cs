using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Random = UnityEngine.Random;
using static Jineni.Personality.PersonalityType;
using Jineni.Personality.types;

namespace Jineni.Personality
{

    public class MBTFunctionValues
    {
        public Dictionary<MBTFunctionsName, MBTFunctions> list;

        public MBTFunctionValues()
        {
            this.list = MBTFunctionsFactory.CreateList();
        }

        public MBTFunctions GetFunctionByName(MBTFunctionsName name)
        {
            if (!list.ContainsKey(name))
            {
                string errMsg = "no " + name + " function in list";
                Debug.LogError(errMsg);
                throw new Exception(errMsg);
            }
            return list[name];
        }


        public void BoostFunctionRandomly(MBTFunctionsName name)
        {
            GetFunctionByName(name).BoostRamdomly();
        }

        public void BoostFunction(MBTFunctionsName name, float value = -1)
        {
            if(value == -1)
            {
                value = UnityEngine.Random.value;
            }
            GetFunctionByName(name).BoostIntensity(value);
        }

        public MBTFunctionsName GetPosition1()
        {
            MBTFunctionsName name = list.
                OrderByDescending(x => x.Value.intensity).
                Where(x => x.Key == MBTFunctionsName.I || x.Key == MBTFunctionsName.E)
                .FirstOrDefault().Key;
            return name;
        }

        public MBTFunctionsName GetPosition2()
        {
            MBTFunctionsName name = list.
                OrderByDescending(x => x.Value.intensity).
                Where(x => x.Key == MBTFunctionsName.N || x.Key == MBTFunctionsName.S)
                .FirstOrDefault().Key;
            return name;
        }
        public MBTFunctionsName GetPosition3()
        {
            MBTFunctionsName name = list.
                OrderByDescending(x => x.Value.intensity).
                Where(x => x.Key == MBTFunctionsName.F || x.Key == MBTFunctionsName.T)
                .FirstOrDefault().Key;
            return name;
        }
        public MBTFunctionsName GetPosition4()
        {
            MBTFunctionsName name = list.
                OrderByDescending(x => x.Value.intensity).
                Where(x => x.Key == MBTFunctionsName.J || x.Key == MBTFunctionsName.P)
                .FirstOrDefault().Key;
            return name;
        }

        public MBTPersonalityType GetMostProminentPersonType()
        {
            MBTFunctionsName[] functions = new MBTFunctionsName[4] {
                GetPosition1(),
                GetPosition2(),
                GetPosition3(),
                GetPosition4()
            };
            //string typeName = string.Concat(functions);
            return new MBTPersonalityType(functions);
        }
    }

    public abstract class MBTFunctions
    {
        public int index;
        public int position;
        public float intensity;
        public MBTFunctions(int index, int position, int value)
        {
            this.index = index;
            this.position = position;
            this.intensity = value;
        }

        public void BoostIntensity(float value)
        {
            intensity += value;
        }

        public void BoostRamdomly()
        {
            intensity += UnityEngine.Random.value;
        }


    }

    public static class MBTFunctionsFactory
    {
        public static Dictionary<PersonalityType.MBTFunctionsName, MBTFunctions> CreateList()
        {
            var possibleNames = System.Enum.GetNames(typeof(PersonalityType.CogFunctionName));
            return new Dictionary<PersonalityType.MBTFunctionsName, MBTFunctions>
            {
                { PersonalityType.MBTFunctionsName.I, new I() },
                { PersonalityType.MBTFunctionsName.E, new E() },
                { PersonalityType.MBTFunctionsName.N, new N() },
                { PersonalityType.MBTFunctionsName.S, new S() },
                { PersonalityType.MBTFunctionsName.T, new T() },
                { PersonalityType.MBTFunctionsName.F, new F() },
                { PersonalityType.MBTFunctionsName.J, new J() },
                { PersonalityType.MBTFunctionsName.P, new P() },
            };
        }
    }

    //TODO only abstract clas with .name?
    public class I : MBTFunctions
    {
        public const int INDEX = 0;
        public const int POSITION = 0;
        public I(int value = 0) : base(INDEX, POSITION, value)
        {
        }
    }
    public class E : MBTFunctions
    {
        public const int INDEX = 1;
        public const int POSITION = 0;
        public E(int value = 0) : base(INDEX, POSITION, value)
        {
        }
    }
    public class N : MBTFunctions
    {
        public const int INDEX = 2;
        public const int POSITION = 1;
        public N(int value = 0) : base(INDEX, POSITION, value)
        {
        }
    }
    public class S : MBTFunctions
    {
        public const int INDEX = 3;
        public const int POSITION = 1;
        public S(int value = 0) : base(INDEX, POSITION, value)
        {
        }
    }
    public class T : MBTFunctions
    {
        public const int INDEX = 4;
        public const int POSITION = 2;
        public T(int value = 0) : base(INDEX, POSITION, value)
        {
        }
    }
    public class F : MBTFunctions
    {
        public const int INDEX = 5;
        public const int POSITION = 2;
        public F(int value = 0) : base(INDEX, POSITION, value)
        {
        }
    }
    public class J : MBTFunctions
    {
        public const int INDEX = 6;
        public const int POSITION = 3;
        public J(int value = 0) : base(INDEX, POSITION, value)
        {
        }
    }
    public class P : MBTFunctions
    {
        public const int INDEX = 7;
        public const int POSITION = 3;
        public P(int value = 0) : base(INDEX, POSITION, value)
        {

        }
    }
}