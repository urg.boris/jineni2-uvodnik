﻿using Jineni.Personality.types;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Jineni.Personality.UIDebug

{
    public class PersonalityDebug:MonoBehaviour
    {
        public List<FunctionDebug> functionDebugs;
        int cogStartIndex = 0;
        int mbtStartIndex = 8;
        PersonalityType personalityType;

        public void Set(PersonalityType personalityType)
        {
            this.personalityType = personalityType;
            SetMBT(personalityType.mbtFunctionsValues, mbtStartIndex);
            SetCog(personalityType.cogFunctionsValues, cogStartIndex);
        }

        void SetMBT(MBTFunctionValues functions, int startIndex)
        {
            var index = startIndex;
            foreach (var function in functions.list)
            {
                functionDebugs[index].ShowDebug(function.Key.ToString(), function.Value.intensity.ToString());
                index++;
            }
        }

        void SetCog(CogFunctionValues functions, int startIndex)
        {
            var index = startIndex;
            foreach (var function in functions.list)
            {
                functionDebugs[index].ShowDebug(function.Key.ToString(), function.Value.intensity.ToString());
                index++;
            }
        }

        public void GetPersonality()
        {
            MBTPersonalityType mBTPersonalityType = personalityType.mbtFunctionsValues.GetMostProminentPersonType();
            Debug.Log(mBTPersonalityType.name);

            CogPersonalityType cogPersonalityType = personalityType.cogFunctionsValues.GetMostProminentPersonType();
            Debug.Log(cogPersonalityType.name);

        }


    }
}
