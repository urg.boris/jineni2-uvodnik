﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Jineni.Personality.UIDebug

{
    public class FunctionDebug: MonoBehaviour
    {
        public Text nameUI;
        public Text valueUI;
        public void ShowDebug(string name, string value)
        {
            nameUI.text = name;
            valueUI.text = value;
        }
    }


}
