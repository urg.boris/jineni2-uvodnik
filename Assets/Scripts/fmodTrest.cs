﻿using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;

namespace Jineni
{

    public class fmodTrest : MonoBehaviour
    {
        public StudioEventEmitter intro;

        public StudioEventEmitter sebastianIntro;
        public StudioEventEmitter pick;

        private float nextRandomPickTime;
        private Coroutine sebastianSound;

        private float sebastianStartTime;

        public float sebastianDuration;
        public float sebastianGapTime;

        private float minSebastianVolume;

        // Start is called beforeText the first frame update
        void Start()
        {
            SetSebastianVolume(0.9f);
            sebastianIntro.Play();
            sebastianIntro.EventInstance.setTimelinePosition(0);
            sebastianIntro.EventInstance.setVolume(0);
            
            intro.Play();
                        
            //SetNextSebstianTime();
            sebastianStartTime = Time.time;
        }

        public void SetSebastianVolume(float volume)
        {
            Debug.Log("set volume="+volume);
            minSebastianVolume = Mathf.Clamp01(volume);
        }

        void SetNextRandomPickTime()
        {
           // nextRandomPickTime = Time.time+5;

        }
        void SetNextSebstianTime()
        {
            sebastianStartTime = Time.time+sebastianGapTime;

        }

        public void ChoicePicked()
        {
            
        }
        public void StartSebastian()
        {
            if (sebastianSound != null) StopCoroutine(sebastianSound);
            sebastianSound = StartCoroutine(PlaySebastian(sebastianDuration));
        }

        IEnumerator PlaySebastian(float duration)
        {
            float passedTime = 0;
            while (passedTime < duration)
            {
                var ratio = passedTime / duration;
                //var volume1 = - Mathf.Pow((2*ratio-1),6)+1;
                var volume2 = Mathf.Max(Mathf.Sin(Mathf.PI * ratio), minSebastianVolume);
                var itch = Mathf.Max(Mathf.Sin(Mathf.PI * ratio), minSebastianVolume)*1000;
                //Debug.Log(volume2);
                int position;
                sebastianIntro.EventInstance.getTimelinePosition(out position);
               // Debug.Log("pos="+position+" ratio=" + ratio + " volume2=" + volume2);
                
                sebastianIntro.EventInstance.setVolume(volume2);
                //sebastianIntro.EventInstance.setPitch(itch);
                
                passedTime += Time.deltaTime;
                yield return null;
            }
            
        }

        // Update is called once per frame
        void Update()
        {

            //var volume1 = (-Mathf.Sin(Time.time * 0.5f) + 1) * 0.5f;
            //var volume2 = (Mathf.Sin(Time.time * 0.5f) + 1) * 0.5f;
            //intro.EventInstance.setVolume(volume1);
           // sebastianIntro.EventInstance.setVolume(volume2);
             if (Time.time > sebastianStartTime)
             {
                 SetNextSebstianTime();
                 StartSebastian();
             }
        }
    }
}