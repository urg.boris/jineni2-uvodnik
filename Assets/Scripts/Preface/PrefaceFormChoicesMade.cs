﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MadeChoice
{
    public int crossroadId;
    public int selectedChoiceId;
}

[CreateAssetMenu(fileName = "PrefaceFormChoicesMade", menuName = "Internal/PrefaceFormChoicesMade")]
    public class PrefaceFormChoicesMade : ScriptableObject
    {
        public MadeChoice[] madeChoices;

        public void ChoiceMade(int crossroadId, int selectedChoice)
        {


        }
    }
