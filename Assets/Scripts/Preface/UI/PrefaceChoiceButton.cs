using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Jineni.Preface
{
    public class PrefaceChoiceButton : MonoBehaviour
    {
        public TMP_Text choice;
        public Button button;
        public Image background;
        private Coroutine currentCorutine;
        private Color baseBackgroundColor;
        [FormerlySerializedAs("prefaceMonoCrossroad")] [FormerlySerializedAs("prefaceCrossroad")] [HideInInspector]
        public PrefaceMonoCrossroad parent;
        // Start is called beforeText the first frame update
        private void Awake()
        {
            baseBackgroundColor = background.color;
        }

        public void Set(PrefaceMonoCrossroad parent, string text, UnityAction onClickCallback)
        {
            choice.text = text;
            button.onClick.AddListener(onClickCallback);
            this.parent = parent;
            //Appear();

        }

        public void Clean()
        {
            button.onClick.RemoveAllListeners();
        }

        public void ForceStopCorutines()
        {
            if (currentCorutine != null) StopCoroutine(currentCorutine);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }
        public void Hide()
        {
            gameObject.SetActive(false);
        }



        public void Appear()
        {
            ForceStopCorutines();
            Show();
            currentCorutine = StartCoroutine(Appearing(2));
        }
        
        public void DisAppear()
        {
            ForceStopCorutines();
            Show();
            button.onClick.RemoveAllListeners();
            currentCorutine = StartCoroutine(Disappearing(1));
        }

        IEnumerator Appearing(float fadeDuration)
        {
            ForceStopCorutines();
            Show();
            choice.alpha = 0;
            Color backgroundColor = baseBackgroundColor;
            backgroundColor.a = 0;
            background.color = backgroundColor;
            float elapsedTime = 0f;
            float ratio;
            while (elapsedTime < fadeDuration)
            {
                elapsedTime += Time.deltaTime;
                ratio = elapsedTime / fadeDuration;
                choice.alpha = ratio;
                backgroundColor.a = ratio;
                background.color = backgroundColor;
                yield return null;
            }
            background.color = baseBackgroundColor;
            choice.alpha = 1;
            parent.FirstChoiseFinnishedAppearing();
        }
        
        
        IEnumerator Disappearing(float fadeDuration)
        {
            ForceStopCorutines();
            Show();
            choice.alpha = 1;
            Color backgroundColor = baseBackgroundColor;
            backgroundColor.a = 1;
            background.color = backgroundColor;
            float elapsedTime = 0f;
            float ratio;
            while (elapsedTime < fadeDuration)
            {
                elapsedTime += Time.deltaTime;
                ratio = 1 - elapsedTime / fadeDuration;
                choice.alpha = ratio;
                backgroundColor.a = ratio;
                background.color = backgroundColor;
                yield return null;
            }
            backgroundColor.a = 1;
            background.color = backgroundColor;
            choice.alpha = 1;
            Hide();
            parent.ChiceOptionDisappeared();
        }
    }
}
