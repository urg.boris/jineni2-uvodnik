using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Preface.Structure;
using Jineni.Personality.UIDebug;
using Jineni.Personality;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Jineni.Preface
{
    /*
    public class ChoiceForUI
    {
        public string text;
        public PrefaceFunctionBoost[] functionsBoost;
    }

    public class CrossroadForUI
    {
        public ChoiceForUI[] choices;
        public string before;
        public string after;
    }*/

    public class PrefaceMonoScreen : MonoBehaviour
    {
        [HideInInspector] public PersonalityController personalityController;
        [HideInInspector] public PrefaceMonoForm parent;
       // [FormerlySerializedAs("prefaceCrossroad1")] public PrefaceMonoCrossroad prefaceMonoCrossroad1;
       // [FormerlySerializedAs("prefaceCrossroad2")] public PrefaceMonoCrossroad prefaceMonoCrossroad2;
        public PrefaceMonoCrossroad[] prefaceMonoCrossroads;

        private PrefaceMonoCrossroad currentCrossroad;
        private int currentCrossroadIndex;
        private enum AppearState
        {
            BeforeAppear,
            CrossroadAppear,
            CrossroadNext,
            AllCrossroadAppeared
        }

        private AppearState currentAppearState;


        private void Awake()
        {
            //prefaceMonoCrossroads = new PrefaceMonoCrossroad[] {prefaceMonoCrossroad1, prefaceMonoCrossroad2};
        }

        public void Set(PrefaceMonoForm parent, PersonalityController personalityController, PrefaceCrossroad[] prefaceCrossroads)
        {
            this.parent = parent;
            this.personalityController = personalityController;
            //Debug.Log(prefaceMonoCrossroads.Length+"  ou");
            for (int i = 0; i < this.prefaceMonoCrossroads.Length; i++)
            {
                prefaceMonoCrossroads[i].Set(this, personalityController, prefaceCrossroads[i]);
                prefaceMonoCrossroads[i].Hide();
            }
        }

        public void Apear()
        {
            Show();
            currentAppearState = AppearState.BeforeAppear;
            HideAllCrossroads();
            currentCrossroad = prefaceMonoCrossroads[0];
            currentCrossroad.Show();
            //LayoutRebuilder.ForceRebuildLayoutImmediate(currentCrossroad.GetComponent<RectTransform>());
            AppearNext();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
        
        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void HideAllCrossroads()
        {
            foreach (var crossroad in prefaceMonoCrossroads)
            {
                crossroad.Hide();
            }
        }

        public void AllCrossroadsFinnished()
        {
            Debug.Log("AllCrossroadsFinnished");
            parent.AppearNext();
        }
        
        public void AppearNext()
        {
//            Debug.Log(currentAppearState);
            if (currentAppearState == AppearState.AllCrossroadAppeared)
            {
                /*currentCrossroadIndex += 1;
                Debug.Log("next cross");
                if (currentCrossroadIndex >= prefaceMonoCrossroads.Length)
                {
                    AllCrossroadsFinnished();
                    return;
                }
                else
                {
                    currentCrossroad = prefaceMonoCrossroads[currentCrossroadIndex];
                    return;   
                }*/
                return;  
            }

            if (currentAppearState == AppearState.BeforeAppear)
            {
                currentAppearState = AppearState.CrossroadAppear;
                currentCrossroadIndex = 0;
                currentCrossroad = prefaceMonoCrossroads[currentCrossroadIndex];

            }

            if (currentAppearState == AppearState.CrossroadAppear)
            {
                AppearCrossroad(currentCrossroad);
                currentAppearState += 1;
                return;
            }

            if (currentAppearState == AppearState.CrossroadNext)
            {
                currentCrossroadIndex += 1;
                if (currentCrossroadIndex >= prefaceMonoCrossroads.Length)
                {
                    AllCrossroadsFinnished();
                    return;
                }
                else
                {
                    currentCrossroad = prefaceMonoCrossroads[currentCrossroadIndex];
                    AppearCrossroad(currentCrossroad);
                    return;
                }
            }

        }

        public void ShowCrossroad(PrefaceMonoCrossroad monoCrossroad)
        {
            monoCrossroad.gameObject.SetActive(true);
        }

        public void HideCrossroad(PrefaceMonoCrossroad monoCrossroad)
        {
            monoCrossroad.gameObject.SetActive(false);
        }

        public void AppearCrossroad(PrefaceMonoCrossroad monoCrossroad)
        {
            ShowCrossroad(monoCrossroad);
            monoCrossroad.Apear();
            //LayoutRebuilder.ForceRebuildLayoutImmediate(monoCrossroad.GetComponent<RectTransform>());
        }

        public void DisAppearCrossroad(PrefaceMonoCrossroad monoCrossroad)
        {
            ShowCrossroad(monoCrossroad);
            monoCrossroad.Disapear();
        }

        public void Disapear()
        {
            foreach (var crossroad in prefaceMonoCrossroads)
            {
                crossroad.Disapear();
            }
            //todo plynulost
            gameObject.SetActive(false);
        }

    }
}