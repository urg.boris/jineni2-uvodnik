using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using plesnif.utils;
using Jineni.Preface.Structure;
using Jineni.Personality.UIDebug;
using Jineni.Personality;
using UnityEngine.UI;

namespace Jineni.Preface
{

    public class PrefaceMonoForm : MonoBehaviour
    {
        public PrefaceMonoScreen[] prefaceMonoScreens;
        public PersonalityController personalityController;

        public PrefaceFormData formData;

        public fmodTrest fmod;
        
        private PrefaceMonoScreen currentScreen;
        private int currentScreenIndex;
        void Start()
        {

            HideAllScreens();

           // formData = GetFormData();
            var conflod = new ConfigLoader("Configs/");
           // conflod.SaveConfig(formData, "prefaceForm.json");
            conflod.LoadConfig(formData, "prefaceForm.json");
            for (int screenIndex = 0; screenIndex < formData.screens.Length; screenIndex++)
            {
                var currentPrefaceScreen = formData.screens[screenIndex];
                var currentMonoScreen = prefaceMonoScreens[screenIndex];
                
                currentMonoScreen.Set(this, personalityController, currentPrefaceScreen.crossroads);

            }

            currentScreen = prefaceMonoScreens[0];
            currentScreen.Apear();

        }
        
        void HideAllScreens()
        {
            foreach (var screen in prefaceMonoScreens)
            {
                screen.Hide();
            }
        }

        public void SoundChoicesAppeared()
        {
            fmod.ChoicePicked();
        }

        public void AllScreensFinnished()
        {
            Debug.Log("AllScreensFinnished");
        }

        public void AppearNext()
        {
            currentScreen.Disapear();
            
           // Debug.Log("sc disap");
            
            currentScreenIndex += 1;
            fmod.SetSebastianVolume(((float)currentScreenIndex/(float)prefaceMonoScreens.Length)*0.9f);
            if (currentScreenIndex >= prefaceMonoScreens.Length)
            {
                AllScreensFinnished();
                return;
            }
            else
            {
                currentScreen = prefaceMonoScreens[currentScreenIndex];
                currentScreen.Apear();
               // LayoutRebuilder.ForceRebuildLayoutImmediate(currentScreen.GetComponent<RectTransform>());
                return;
            }
        }
        
        void Update()
        {
            if (Input.GetKeyUp(KeyCode.A))
            {
                for (int screenIndex = 0; screenIndex < formData.screens.Length; screenIndex++)
                {
                    prefaceMonoScreens[screenIndex].Apear();
                }
            }
        }

        PrefaceFormData GetFormData()
        {
            var cross1 = GetTestCross1();
            var cross2 = GetTestCross2();
            var screen = new Structure.PrefaceScreen("screen1", new Structure.PrefaceCrossroad[] { cross1, cross2 });
            var form = ScriptableObject.CreateInstance<PrefaceFormData>();
            form.Init(new Structure.PrefaceScreen[] { screen });
            return form;
        }

        Structure.PrefaceCrossroad GetTestCross1()
        {
            var functionBoosts = new PrefaceFunctionBoost[] {
                new PrefaceFunctionBoost("Fe", 0.5f),
                new PrefaceFunctionBoost("Fe", 0.5f)
            };

            var pChoices = new PrefacePickableChoice[] {
                new PrefacePickableChoice(new[] {"Tak ja ti reknu jak to vsechno zacalo"}, null, functionBoosts),
                new PrefacePickableChoice(new[] {"Tenkrat sem jeste netusil, ze mi tahle drobnost zmeni cely zivotni"}, null, functionBoosts),
                new PrefacePickableChoice(new[] {"to co sem do tedoby zazil me nemohlo pripravit na to co nasledovalo"}, null, functionBoosts),
                new PrefacePickableChoice(new[] {"dyz na to koukam zpetne, stale to nechapu"}, null, functionBoosts),
                new PrefacePickableChoice(new[] {"To chci vypravet, sem jeste nikomu nerekl"}, null, functionBoosts),
                new PrefacePickableChoice(new[] {"Vlastne vubec nechapu co se to stalo"}, null, functionBoosts),
                new PrefacePickableChoice(new[] {"S odstupem, ani nevim jestli se to se to vubec stalo"}, null, functionBoosts),
        };

            //var preCh = new Choices(pChoices);
            var preTitle = new PrefaceRandomText(new string[] { "tedy..." });
            var aftrTitle = new PrefaceRandomText(new string[] { "..." });

            var cros = new Structure.PrefaceCrossroad("cross1", pChoices, preTitle, aftrTitle);
            return cros;

        }

        Structure.PrefaceCrossroad GetTestCross2()
        {

            var functionBoost1 = new PrefaceFunctionBoost("T", 0.5f);
            var functionBoost2 = new PrefaceFunctionBoost("Ni", 0.5f);
            var functionBoosts = new PrefaceFunctionBoost[] { functionBoost1, functionBoost2 };

            var choice1 = new PrefacePickableChoice(new[] {"rict neco konkretniho"}, new string[] { "14.ledna", "byl hnedy" }, functionBoosts);
            var choice2 = new PrefacePickableChoice(new[] {"rict neco nejasneho"} ,new string[] { "jako kdyz mnes jemny papir", "zpoza rohu" });
            var pChoices = new PrefacePickableChoice[] { choice1, choice2 };

            // var preCh = new Choices(pChoices);
            var preTitle = new PrefaceRandomText(new string[] { "ale jak se k tomu postavit?", "co stim?" });
            var aftrTitle = new PrefaceRandomText(new string[] { "..tak nejak to bylo" });

            var cros = new Structure.PrefaceCrossroad("cross2", pChoices, preTitle, aftrTitle);
            return cros;

        }

        // Update is called once per frame

    }
}
