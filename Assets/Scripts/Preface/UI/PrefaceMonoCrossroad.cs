﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using Jineni.Preface.Structure;
using Jineni.Personality.UIDebug;
using Jineni.Personality;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Jineni.Preface
{
    public class PrefaceMonoCrossroad : MonoBehaviour
    {
        public TMP_Text beforeText;
        public TMP_Text choiceText;

        public TMP_Text afterText;

        // public RectTransform viewPort;
        public RectTransform choicesBox;

        // public RectTransform scrollview;
        [HideInInspector]
        public PrefaceChoiceButton[] choices;

        public RectTransform order;
        //public PrefacePickableChoice 
        public GameObject choisePrefab;
        [HideInInspector] public PersonalityController personalityController;
        public Coroutine currentCorutine { get; set; }
        [HideInInspector] public PrefaceMonoScreen parent;
        private PrefaceCrossroad prefaceCrossroad;
        private bool areChoicesDisappeared;
        private enum AppearState
        {
            BeforeAppear,
            ChoiceIntro,
            ViewingChoices,
            ChoiceFeedback,
            ChoiceOutro,
            AllAppeared
        }

        private AppearState currentAppearState;
        private bool choisesFinnishedAppearing;
        private int pickedChoiseId;

        public delegate void CommentMethod(string text);

        public void Set(PrefaceMonoScreen parent, PersonalityController personalityController, PrefaceCrossroad prefaceCrossroad)
        {
            this.parent = parent;
            this.personalityController = personalityController;

            beforeText.transform.SetAsFirstSibling();
            this.prefaceCrossroad = prefaceCrossroad;
            choices = new PrefaceChoiceButton[this.prefaceCrossroad.choices.Length];
            for (var index = 0; index < this.prefaceCrossroad.choices.Length; index++)
            {
                var choiceText = this.prefaceCrossroad.choices[index];
                var choiceButtonPrefab = Instantiate(choisePrefab, choicesBox.transform);

                var choiceButtonScript = choiceButtonPrefab.GetComponent<PrefaceChoiceButton>();
                var preventMutabilityIndex = index;
                choiceButtonScript.Set(this, choiceText.GetBeforePickedText(),
                    delegate { ChoiseMade(preventMutabilityIndex, choiceText.functionsBoost); });
                choiceButtonPrefab.GetComponent<RectTransform>().ForceUpdateRectTransforms();
                var buttonHeight = choiceButtonPrefab.GetComponent<RectTransform>().rect.height;
                choices[index] = choiceButtonScript;
            }

            afterText.transform.SetAsLastSibling();
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void ChoiseMade(int id, PrefaceFunctionBoost[] functionsBoost)
        {
            pickedChoiseId = id;
            Debug.Log("choise made now id="+pickedChoiseId);
            personalityController.BoostFunctions(functionsBoost);
            foreach (var choice in choices)
            {
                choice.DisAppear();
            }

            currentAppearState = AppearState.ChoiceFeedback;
            AppearNext();
            
        }

        public void Apear()
        {
            currentAppearState = AppearState.BeforeAppear;
            HideText(beforeText);
            HideText(choiceText);
            HideText(afterText);
            foreach (var choice in choices)
            {
                choice.Hide();
            }
            choicesBox.gameObject.SetActive(false);

            AppearNext();
        }

        public void Disapear()
        {
            DisappearText(beforeText);
            DisappearText(choiceText);
            DisappearText(afterText);
        }

        public void ChiceOptionDisappeared()
        {
            if (!areChoicesDisappeared)
            {
                areChoicesDisappeared = true;
                choicesBox.gameObject.SetActive(false);
            }
        }
        

        public void AppearNext()
        {
          //  Debug.Log(currentAppearState);
            if (currentAppearState == AppearState.AllAppeared)
            {
                ForceStopCorutines();
                parent.AppearNext();
                return;
            }

            if (currentAppearState == AppearState.BeforeAppear)
            {
                currentAppearState = AppearState.ChoiceIntro;
            }

            if (currentAppearState == AppearState.ChoiceIntro)
            {
                currentAppearState += 1;
                var text = prefaceCrossroad.beforeText.GetText();

                if (!String.IsNullOrEmpty(text))
                {
                    beforeText.text = text;
                    AppearText(beforeText);
                }
                else
                {
                    AppearNext();
                }

                return;
            }

            if (currentAppearState == AppearState.ViewingChoices)
            {
                choicesBox.gameObject.SetActive(true);
                areChoicesDisappeared = false;
                foreach (var choice in choices)
                {
                    choice.Appear();
                    
                    //LayoutRebuilder.ForceRebuildLayoutImmediate(choicesBox);
                    //choicesBox.ForceUpdateRectTransforms();
                }
                RebuildOptionsUI();


                // LayoutRebuilder.ForceRebuildLayoutImmediate(order);
                //LayoutRebuilder.ForceRebuildLayoutImmediate(parent.GetComponent<RectTransform>());
               // Canvas.ForceUpdateCanvases();
                //LayoutRebuilder.ForceRebuildLayoutImmediate(choicesBox);

                parent.parent.SoundChoicesAppeared();

                return;
            }

            if (currentAppearState == AppearState.ChoiceFeedback)
            {
                currentAppearState += 1;
                var text = prefaceCrossroad.GetPickedChoiceAfterText(pickedChoiseId);
                //Debug.Log("ftext="+text);
                if (!String.IsNullOrEmpty(text))
                {
                   // Debug.Log("not null="+text);
                    choiceText.text = text;
                    AppearText(choiceText);
                }
                else
                {
                    text = prefaceCrossroad.GetPickedChoiceBeforeText(pickedChoiseId);
                    //Debug.Log("is null(get before)="+text);
                    
                    choiceText.text = text;
                    AppearText(choiceText);
                }

                return;
            }

            if (currentAppearState == AppearState.ChoiceOutro)
            {
                currentAppearState += 1;
                var text = prefaceCrossroad.afterText.GetText();
          
                if (!String.IsNullOrEmpty(text))
                {
                    //Debug.Log("not null="+text);
                    afterText.text = text;
                    AppearText(afterText);
                }
                else
                {
                    AppearNext();
                }

                return;
            }
        }

        void RebuildOptionsUI()
        {
            StartCoroutine(RebuildOptionsUIInAWhile());
        }
        IEnumerator RebuildOptionsUIInAWhile()
        {
            yield return new WaitForEndOfFrame();
            LayoutRebuilder.ForceRebuildLayoutImmediate(choicesBox);
            LayoutRebuilder.ForceRebuildLayoutImmediate(order);
        }


        public void FirstChoiseFinnishedAppearing()
        {
            if (choisesFinnishedAppearing == false)
            {
                choisesFinnishedAppearing = true;
                /*choicesBox.gameObject.SetActive(false);
                choicesBox.gameObject.SetActive(true);*/
            }
        }

        public void ForceStopCorutines()
        {
            if (currentCorutine != null) StopCoroutine(currentCorutine);
        }

        public void ShowText(TMP_Text text)
        {
            text.gameObject.SetActive(true);
        }

        public void HideText(TMP_Text text)
        {
            text.gameObject.SetActive(false);
        }

        public void AppearText(TMP_Text text)
        {
            
            currentCorutine = StartCoroutine(Appearing(2, text));
        }

        public void DisappearText(TMP_Text text)
        {
          // Debug.Log("text"+text.name+"is "+text.gameObject.activeSelf);
            if (text.gameObject.activeSelf)
            {
                currentCorutine = StartCoroutine(Disappearing(2, text));
            }
        }

        IEnumerator Appearing(float fadeDuration, TMP_Text text)
        {
            ForceStopCorutines();
            ShowText(text);
            text.alpha = 0;
            float elapsedTime = 0f;
            float ratio;
            while (elapsedTime < fadeDuration)
            {
                elapsedTime += Time.deltaTime;
                ratio = elapsedTime / fadeDuration;
                text.alpha = ratio;
                yield return null;
            }

            text.alpha = 1;
            AppearNext();
        }


        IEnumerator Disappearing(float fadeDuration, TMP_Text text)
        {
            ForceStopCorutines();
            ShowText(text);
            text.alpha = 1;
            float elapsedTime = 0f;
            float ratio;
            while (elapsedTime < fadeDuration)
            {
                elapsedTime += Time.deltaTime;
                ratio = elapsedTime / fadeDuration;
                text.alpha = ratio;
                yield return null;
            }

            text.alpha = 1;
            HideText(text);
        }

        bool IsEmpty(string text)
        {
            return string.IsNullOrEmpty(text);
        }
    }
}