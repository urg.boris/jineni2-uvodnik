﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Jineni.Preface.Structure
{
    [Serializable]
    public class PrefaceFunctionBoost
    {
        public string name;
        public float value;

        public PrefaceFunctionBoost(string name, float value)
        {
            this.name = name;
            this.value = value;
        }
    }

    [Serializable]
    public class PrefaceRandomText
    {
        public string[] text;

        public PrefaceRandomText(string[] text)
        {
            this.text = text;
        }

        public string GetText()
        {
            if (text == null || text.Length == 0)
            {
                return null;
            }

            int rndIndex = Random.Range(0, text.Length);
            return text[rndIndex];
        }
    }
/*
    [Serializable]
    class OptionText
    {
        public string[] beforePicked;
        public string[] afterPicked;
    }*/

    [Serializable]
    public class PrefacePickableChoice
    {
        public int id;
        public PrefaceRandomText beforeText;
        public PrefaceRandomText afterText;
        public PrefaceFunctionBoost[] functionsBoost;

        public PrefacePickableChoice(string[] beforeText, string[] afterText=null, PrefaceFunctionBoost[] functionsBoost = null)
        {
            this.beforeText = new PrefaceRandomText(beforeText);
            this.afterText = new PrefaceRandomText(afterText);
            this.functionsBoost = functionsBoost;
        }

        string GetText(PrefaceRandomText textOptions)
        {
            return textOptions.GetText();
        }
        public string GetBeforePickedText()
        {
            return GetText(beforeText);
        }
        public string GetAfterPickedText()
        {
            return GetText(afterText);
        }
    }

    /*  [Serializable]
      class Choices
      {
          public PrefacePickableChoice[] pickableChoices;
  
          public Choices(PrefacePickableChoice[] pickableChoices)
          {
              this.pickableChoices = pickableChoices;
          }
  
          public string[] GetChoicesAsText()
          {
              var texts = new string[pickableChoices.Length];
              for (int i = 0; i < pickableChoices.Length; i++)
              {
                  texts[i] = pickableChoices[i].GetBeforePickedText();
              }
              return texts;
  
          }
      }*/
    [Serializable]
    public class PrefaceCrossroad
    {
        public string id;

        //PrefaceQuestion can be notnig
        public PrefaceRandomText beforeText;
        public PrefaceRandomText afterText;
        public PrefacePickableChoice[] choices;
        public bool shuffleChoices;

        public PrefaceCrossroad(string id, PrefacePickableChoice[] choices, PrefaceRandomText beforeText = null,
            PrefaceRandomText afterText = null)
        {
            this.beforeText = beforeText;
            for (var index = 0; index < choices.Length; index++)
            {
                var choice = choices[index];
                choice.id = index;
            }

            this.choices = choices;
            this.afterText = afterText;
            this.id = id;
           
        }

        public string[] GetChoicesAsText()
        {
            if (choices == null || choices.Length == 0)
            {
                return null;
            }

            var texts = new string[choices.Length];
            for (int i = 0; i < choices.Length; i++)
            {
                texts[i] = choices[i].GetBeforePickedText(); // GetBeforePickedText();
            }

            return texts;
        }
        
        public string GetPickedChoiceAfterText(int choiceId)
        {
            if (choices == null || choices.Length == 0)
            {
                return null;
            }

            var pickedChoise = choices[choiceId];
            var afterPickedText = pickedChoise.GetAfterPickedText();
            return afterPickedText;
        }
        public string GetPickedChoiceBeforeText(int choiceId)
        {
            if (choices == null || choices.Length == 0)
            {
                return null;
            }

            var pickedChoise = choices[choiceId];
            //TODO mozna hlidat jaky text to byl, audio a tak
            var afterPickedText = pickedChoise.GetBeforePickedText();
            return afterPickedText;
        }
    }

    [Serializable]
    public class PrefaceScreen
    {
        public string id;
        public PrefaceCrossroad[] crossroads;

        public PrefaceScreen(string id, PrefaceCrossroad[] prefaceCrossroads)
        {
            this.id = id;
            this.crossroads = prefaceCrossroads;
        }
    }
    [CreateAssetMenu(fileName = "PrefaceFormData", menuName = "Internal/PrefaceFormData")]
    public class PrefaceFormData : ScriptableObject
    {
        public PrefaceScreen[] screens;

        public void Init(PrefaceScreen[] prefaceScreens)
        {
            this.screens = prefaceScreens;
        }
    }
}