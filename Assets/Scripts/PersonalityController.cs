using Jineni.Personality.UIDebug;
using Jineni.Preface.Structure;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jineni.Personality {
    public class PersonalityController : MonoBehaviour
    {
        // Start is called beforeText the first frame update
        public PersonalityType personalityType = new PersonalityType();
        public PersonalityDebug personalityDebug;

        [ContextMenu("Do Something")]
        void DoSomething()
        {
            personalityType.Do();
            personalityDebug.Set(personalityType);
        }

        void Start()
        {
            DoSomething();
            personalityDebug.gameObject.SetActive(false);
        }

        public void BoostFunctions(PrefaceFunctionBoost[] functionsBoost)
        {
            personalityType.BoostFunctions(functionsBoost);
            personalityDebug.Set(personalityType);
        }
  

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyUp(KeyCode.D))
            {
                personalityDebug.gameObject.SetActive(!personalityDebug.gameObject.activeSelf);
            }
        }
    }
}
